const functions = require("firebase-functions");
const express = require("express");
const app = express();

const admin = require("firebase-admin");
const serviceAccount = require("./permisos.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

app.use(require("./routes/productos.routes"));
app.use(require("./routes/eventos.routes"));

exports.app = functions.https.onRequest(app);