const {Router} = require("express");
const router = Router();


var admin = require("firebase-admin");
const db = admin.firestore();



//envio informacion al sistema
router.post("/eventos",async(req,res)=>{
    try {
        await db.collection("eventos")
        .doc("/"+req.body.local+req.body.fecha+"/")
        .create({
            fecha: req.body.fecha,
            hora: req.body.hora,
            local: req.body.local,
            direccion: req.body.direccion,
            artistas: req.body.artistas,
            departamento: req.body.departamento,
            provincia: req.body.provincia,
            imagen: req.body.imagen,
            tipoEvento: req.body.tipoEvento,
            nombre: req.body.nombre,
            comprar: req.body.comprar
        });
        return res.status(200).json();
    } catch (error) {
        console.log(error);
        return res.status(500).json();
    }
});
//recuperar un evento
router.get("/eventos/:evento_id",async(req,res)=>{
        try {
            const snapshot = await db.collection("eventos").doc(req.params.evento_id).get();
            const userData = snapshot.data();
            return res.status(200).json(userData);
        } catch (error) {
            return res.status(500).send(error);
        }
});
//listar de eventos
router.get("/eventos",async(req,res)=>{
    try {
        const query = db.collection("eventos");
        const querySnapshot = await query.get();
        const documentos = querySnapshot.docs;
        const respuesta = documentos.map(doc => ({
            id: doc.id,
            fecha: doc.data().fecha,
            hora: doc.data().hora,
            local: doc.data().local,
            direccion: doc.data().direccion,
            artistas: doc.data().artistas,
            departamento: doc.data().departamento,
            provincia: doc.data().provincia,
            imagen: doc.data().imagen,
            tipoEvento: doc.data().tipoEvento,
            nombre: doc.data().nombre,
            comprar: doc.data().comprar
        }));
        return res.status(200).json(respuesta);
    } catch (error) {
        return res.status(500).json();
    }
});

//eliminar
router.delete("/eventos/:evento_id",async(req,res)=>{
    try {
        const evento = db.collection("eventos").doc(req.params.evento_id);
        await evento.delete();
        return res.status(200).json();
    } catch (error) {
        return res.status(500).json();
    }
});

//actualizar
router.put("/eventos/:evento_id",async(req,res)=>{
    try {
        const evento = db.collection("eventos").doc(req.params.evento_id);
        await evento.update({
            fecha: req.body.fecha,
            hora: req.body.hora,
            local: req.body.local,
            direccion: req.body.direccion,
            artistas: req.body.artistas,
            departamento: req.body.departamento,
            provincia: req.body.provincia,
            imagen: req.body.imagen,
            tipoEvento: req.body.tipoEvento,
            nombre: req.body.nombre,
            comprar: req.body.comprar
        });
        return res.status(200).json();
    } catch (error) {
        return res.status(500).json();
    }
});

module.exports = router;