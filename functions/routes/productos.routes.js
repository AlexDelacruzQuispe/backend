const {Router} = require ("express");
const router = Router();

var admin = require("firebase-admin");
const db = admin.firestore();

router.post("/api/productos",async(req,res)=>{
    try {
        await db.collection("productos")
        .doc("/"+req.body.id+"/")
        .create({
            name: req.body.name
        })
        return res.status(204).json();
    } catch (error) {
        console.log(error);
        return res.status(500).send(error);
    }
});

router.get("/api/productos/:productos_id",(req,res)=>{
(async () => {
    try {
        const doc = db.collection("productos").doc(req.params.productos_id);
        const item = await doc.get();
        const respuesta = item.data();
        return res.status(200).json(respuesta);
    } catch (error) {
        return res.status(500).send(error);
    }
})();
});

router.get("/api/productos",async(req,res)=>{
    try {
    const query = db.collection("productos");
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const respuesta = docs.map(doc =>({
        id: doc.id,
        name: doc.data().name
    }));
    return res.status(200).json(respuesta);
        
    } catch (error) {
        return res.status(500).json();
    }
});

router.delete("/api/productos/:producto_id",async(req,res)=>{
    try {
        const producto = db.collection("productos").doc(req.params.producto_id);
        await producto.delete();
        return res.status(200).json();
    } catch (error) {
        return res.status(500).json();
    }
});

router.put("/api/productos/:productos_id",async(req,res)=>{
    try {
        const producto = db.collection("productos").doc(req.params.productos_id);
        await producto.update({
            name:req.body.name,
        });
        return res.status(200).json();

    } catch (error) {
        return res.status(500).json();
    }
});

module.exports = router;